﻿using System;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace CaptureRx.SQA.TestRail
{
    public class PostTestResultsClient
    {
        public string StrUser;
        public string StrPassword;
        public string StrUrl;

        public PostTestResultsClient(string baseUrl)
        {
            if (!baseUrl.EndsWith("/"))
            {
                baseUrl += "/";
            }

            StrUrl = baseUrl + "index.php?/api/v2/";
        }

        public string User
        {
            get { return StrUser; }
            set { StrUser = value; }
        }

        public string Password
        {
            get { return StrPassword; }
            set { StrPassword = value; }
        }

       public object SendGet(string uri)
        {
            return SendRequest("GET", uri, null);
        }

        public object SendPost(string uri, object data)
        {
            return SendRequest("POST", uri, data);
        }

        private object SendRequest(string method, string uri, object data)
        {
            string url = StrUrl + uri;

           HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = method;

            string auth = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(
                    $"{StrUser}:{StrPassword}"
                )
            );

            request.Headers.Add("Authorization", "Basic " + auth);

            if (method == "POST")
            {
               if (data != null)
                {
                    byte[] block = Encoding.UTF8.GetBytes(
                        JsonConvert.SerializeObject(data)
                    );

                    request.GetRequestStream().Write(block, 0, block.Length);
                }
            }

            Exception ex = null;
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    throw;
                }

                response = (HttpWebResponse)e.Response;
                ex = e;
            }

            // Read the response body, if any, and deserialize it from JSON.
            string text = "";
            if (response != null)
            {
                var reader = new StreamReader(
                    response.GetResponseStream(),
                    Encoding.UTF8
                );

                using (reader)
                {
                    text = reader.ReadToEnd();
                }
            }

            JContainer result;
            if (text != "")
            {
                if (text.StartsWith("["))
                {
                    result = JArray.Parse(text);
                }
                else
                {
                    result = JObject.Parse(text);
                }
            }
            else
            {
                result = new JObject();
            }

            // Check for any occurred errors and add additional details to
            // the exception message, if any (e.g. the error message returned
            // by TestRail).
            if (ex != null)
            {
                string error = (string)result["error"];
                if (error != null)
                {
                    error = '"' + error + '"';
                }
                else
                {
                    error = "No additional error message received";
                }

                throw new TestRailApiException(
                    $"TestRail API returned HTTP {(int) response.StatusCode} ({error})"
                );
            }
            return result;
        }
    }
   
}
