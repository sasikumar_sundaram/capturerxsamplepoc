﻿using System;
using System.Runtime.Serialization;



namespace CaptureRx.SQA.TestRail
{
    public class TestRailApiException : Exception
    {
        public TestRailApiException()
        {
        }

        public TestRailApiException(string message) : base(message)
        {
        }

        public TestRailApiException(string message,
            Exception innerException) : base(message, innerException)
        {
        }

        protected TestRailApiException(SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
