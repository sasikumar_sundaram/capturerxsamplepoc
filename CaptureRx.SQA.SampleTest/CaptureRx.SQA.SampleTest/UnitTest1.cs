﻿using System;
using System.Collections.Generic;
using CaptureRx.SQA.FileProcessor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CaptureRx.SQA.TestRail;
using System.Configuration;
using Newtonsoft.Json.Linq;

namespace CaptureRx.SQA.SampleTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            FileProcessorEngine objEngine = new FileProcessorEngine();
            var result = objEngine.CopyFile("C:\\CaptureRx\\Source", "C:\\CaptureRx\\Destination", "Test1.txt");

            PostTestResultsClient objClient = new PostTestResultsClient(ConfigurationManager.AppSettings["apiurl"])
            {
                User = ConfigurationManager.AppSettings["apiusername"],
                Password = ConfigurationManager.AppSettings["apipassword"]
            };
            JObject c = (JObject)objClient.SendGet("get_project/19");
            if (result == true)
            {
                var data = new Dictionary<string, object>
                {
                    { "status_id", 1 }, //Failed - 5, Pass - 1, Blocked - 2, Retest -4
                    { "comment", "This one passed" }
                };
                JObject r = (JObject)objClient.SendPost("add_result_for_case/115/54243", data);
            }
            else 
            {
                var data = new Dictionary<string, object>
                {
                    { "status_id", 5 }, //Failed - 5, Pass - 1, Blocked - 2, Retest -4
                    { "comment", "This one failed" }
                };
                JObject r = (JObject)objClient.SendPost("add_result_for_case/115/54243", data);
            }
            Assert.IsTrue(result.Equals(true));
            }
    }
}
